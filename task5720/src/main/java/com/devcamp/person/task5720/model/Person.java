package com.devcamp.person.task5720.model;

public abstract class Person {
    private int age;
    private String gender;
    private String name;
    private Address address;

    abstract void eat();

    public Person() {
        
    }

    public Person(int age, String gender, String name, Address address) {
        this.age = age;
        this.gender = gender;
       this.name = name;
       this.address = address;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    

    
}
