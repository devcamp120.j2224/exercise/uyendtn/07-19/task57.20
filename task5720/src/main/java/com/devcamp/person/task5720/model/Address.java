package com.devcamp.person.task5720.model;

public class Address  {
    String street;
    String city;
    String country;
    int postcode;

    public boolean isValidate() {
        return isValidate();
    }

    public Address() {
    }

    public Address(String street, String city, String country, int postcode) {
        this.street = street;
        this.city = city;
        this.country = country;
        this.postcode = postcode;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getPostcode() {
        return postcode;
    }

    public void setPostcode(int postcode) {
        this.postcode = postcode;
    }

    public String toString() {
        return "Street " + street + " city " + city + " country " + country;
    }
}
