package com.devcamp.person.task5720.model;

import java.util.ArrayList;

public class Student extends Person {
    int studentId;

    ArrayList<Subject> subject;

    @Override
    void eat() {
        // TODO Auto-generated method stub
        System.out.println("student is eating");
    }
    
    public void doHomework() {
        System.out.println("student is doing homework...");
    }

    public ArrayList<Subject> getSubject() {
        return subject;
    }

    public void setSubject(ArrayList<Subject> subject) {
        this.subject = subject;
    }

    public Student(int studentId, ArrayList<Subject> subject) {
        this.studentId = studentId;
        this.subject = subject;
    }

    public Student(int age, String gender, String name, Address address, int studentId, ArrayList<Subject> subject) {
        super(age, gender, name, address);
        this.studentId = studentId;
        this.subject = subject;
        
    }

    public Student() {
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }



}
