package com.devcamp.person.task5720;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task5720Application {

	public static void main(String[] args) {
		SpringApplication.run(Task5720Application.class, args);
	}

}
