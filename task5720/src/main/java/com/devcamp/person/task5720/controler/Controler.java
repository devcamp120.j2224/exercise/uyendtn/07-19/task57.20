package com.devcamp.person.task5720.controler;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.person.task5720.model.Address;
import com.devcamp.person.task5720.model.Person;
import com.devcamp.person.task5720.model.Profesor;
import com.devcamp.person.task5720.model.Student;
import com.devcamp.person.task5720.model.Subject;

@RestController
public class Controler {
    @CrossOrigin
    @GetMapping("listPerson")
    public ArrayList<Person> listPerson() {
        ArrayList<Person> lstPerson = new ArrayList<Person>();

        Address address1 = new Address("Mandison", "Miami", "US", 12314);
       Address address2 = new Address("Mandison", "Miami", "US", 12314);

        Profesor profesorMath = new Profesor();
        profesorMath.setAge(50);
        profesorMath.setGender("male");
        profesorMath.setName("John");
       // profesorMath.setAddress(address1);
        ((Profesor)profesorMath).teaching();

        ArrayList<Subject> listSubject = new ArrayList<Subject>();
        Subject subjectMath = new Subject("Math", 1, (Profesor)profesorMath);
        listSubject.add(subjectMath);

       // Student student1 = new Student(20, "male", "John", address2, 1, listSubject);
      // Person student1 = new Student(1, listSubject);
       // ((Student)student1).doHomework();
       // ((Student)student1).setSubject(listSubject);
       Student student1 = new Student();
      // student1.setAddress(address2);
       student1.setAge(20);
       student1.setGender("male");
       student1.setName("John");
       student1.setStudentId(1);
       student1.setSubject(listSubject);
        //((Student)student1).setSubject(listSubject);
       // student1.setAddress(address2);

        //lstPerson.add(profesorMath);
        lstPerson.add(student1);

       

        return lstPerson;
    }
    

}
